import os.path
from zipfile import ZipFile
global zipDataDir, extDataDir, infoDataDir
global useCache, useZipCache, forceExtract
zipDataDir = './Data/zips/'
xmlDataDir = './Data/xmls/'
infoDataDir = './Data/info/'

useCache = True #if false overrides all other cache-based options (also updates cache)
useZipCache = True
forceExtract = False

if not useCache:
	useZipCache = False
	forceExtract = True

def readFile(fileName):
	ret = ''
	with open(fileName, 'r') as f:
		ret = f.read().replace('\n','')
	return ret

import urllib.request
def downloadToZip(urlArgString):
	global zipDataDir, useZipCache
	url='https://clinicaltrials.gov/ct2/results/download_studies?'+urlArgString
	zipFile=zipDataDir+urlArgString+".zip"
	
	if useZipCache and os.path.exists(zipFile):
		verifyValid = ""
		badZip = False
		try:
			verifyValid = ZipFile(zipFile)
			verifyValid = verifyValid.testzip()
			if verifyValid is not None:
				print(f"[CACHE][WARN][{urlArgString}] Corrupted Zip @ file: {verifyValid}, Redownloading...")
				badZip = True
				pass
		except:
			print(f"[CACHE][WARN][{urlArgString}] Corrupted Zip, Redownloading...")
			badZip = True
			pass
		
		if badZip: pass
		else: return
		print(f"[CACHE][NOTI][{urlArgString}] Skipped Download, already in zip cache")		
	elif useZipCache and infoFileExists(urlArgString):
		print(f"[CACHE][NOTI][{urlArgString}] Skipped Download, not in zip cache, but digest exists")
		return
	
	#Delete zip if it exists
	if os.path.exists(zipFile):
		os.remove(zipFile)
		print(f"[CACHE][WARN][{urlArgString}] Deleted Zip File, Redownloading...")
	
	print(f"[CACHE][INST][{urlArgString}] Downloading...", end = "", flush=True)
	urllib.request.urlretrieve(url, zipFile)
	print(f" | Downloaded")

def infoFileExists(urlArgString):
	global infoDataDir
	infoFile = infoDataDir+urlArgString+".info"
	return os.path.exists(infoFile)

def writeInfoFile(urlArgString, xmlList):
	global infoDataDir
	infoFile = infoDataDir+urlArgString+".info"
	if infoFileExists(urlArgString): print(f"[CACHE][WARN][{urlArgString}] Digest already exists, force updated")
	with open(infoFile, 'w') as iF:
		for xmlFile in xmlList:
			iF.write(xmlFile+'\n')
	print(f"[CACHE][INST][{urlArgString}] Wrote {infoFile}...")

def readInfoFile(urlArgString):
	global infoDataDir
	ret = []
	infoFile = infoDataDir+urlArgString+".info"
	for line in open(infoFile, 'r'):
		ret.append(line.strip())
	print(f"[CACHE][PULL][{urlArgString}] Retrieved xml list")
	return ret

def extractZipAndUpdate(urlArgString):
	global zipDataDir, xmlDataDir, forceExtract
	
	zipFile=zipDataDir+urlArgString+".zip"
	xmlDir=xmlDataDir
	
	if forceExtract or not infoFileExists(urlArgString) or urlArgString[:3] == "id=":
		with ZipFile(zipFile) as z:
			writeInfoFile(urlArgString, z.namelist())
			print(f"[CACHE][PULL][{urlArgString}] Extracting {zipFile} to {xmlDir}...", end = "", flush=True)
			z.extractall(xmlDir)
			print(f" | Extracted")
		return
	print(f"[CACHE][NOTI][{urlArgString}] Skipped zip extraction, digest exists and nonid")


#can also handle individual ids
def xmlExists(xmlString):
	global xmlDataDir
	xmlString = xmlString.replace(".xml","")+'.xml'#can also handle individual ids
	xmlLoc=xmlDataDir+xmlString
	return os.path.exists(xmlLoc)

#can also handle individual ids
def setXML(xmlString):
	global xmlDataDir
	idString = xmlString.replace(".xml","")
	if xmlExists(idString): return
	setCache(f"id={idString}")
	
def setCache(urlArgString):
	if not useCache: print(f"[CACHE][NOTI][{urlArgString}] Update Forced")
	downloadToZip(urlArgString)
	extractZipAndUpdate(urlArgString)
	#print(readInfoFile(urlArgString))

def getXmlDataDir():
	global xmlDataDir
	return xmlDataDir

def getXMLlist(urlArgString):
	setCache(urlArgString)
	return readInfoFile(urlArgString)

#can also handle individual ids
def getXMLString(xmlString):
	global xmlDataDir
	setXML(xmlString) #update if not exists
	xmlString = xmlString.replace(".xml","")+'.xml' #can also handle individual ids
	xmlLoc=xmlDataDir+xmlString
	print(f"[CACHE][PULL][{xmlString}] Retrieved xml string")
	return readFile(xmlLoc)

def main():
	setCache('cond=Psoriasis')

if __name__ == "__main__":
    main()


