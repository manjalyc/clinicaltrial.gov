#!/usr/bin/python3
#Generates master.csv from the feedlist
outputTSV='master.tsv'

keys = [
	['id_info','nct_id'],
	'brief_title',
	'acronym',
	['id_info','org_study_id'],
	['id_info','secondary_id'],
	'overall_status',
	'why_stopped',
	'phase',
	'condition',
	['intervention','intervention_type'],
	['intervention','intervention_name'],
	['sponsors','lead_sponsor','agency'],
	['sponsors','collaborator','agency'],
	'start_date',
	'primary_completion_date',
	'completion_date',
	'study_first_posted',
	'last_update_posted',
	'results_first_posted',
	'study_type',
	'study_design_info',
	'enrollment',
	['eligibility','gender'],
	['eligibility','minimum_age'],
	['eligibility','maximum_age'],
	['primary_outcome','measure'],
	['secondary_outcome','measure'],
	['location','facility','name'],
	]

"""
keys = [
	'official_title',
	'brief_title',
	['id_info','nct_id'],
	['id_info','org_study_id'],
	['id_info','secondary_id'],
	'acronym',
	'condition',
	'overall_status',
	'phase',
	'why_stopped',
	['sponsors','lead_sponsor','agency'],
	['sponsors','collaborator','agency'],
	'start_date',
	'primary_completion_date',
	'completion_date',
	'study_first_posted',
	'last_update_posted',
	'results_first_posted',
	'study_type',
	'study_design_info',
	'enrollment',
	['eligibility','gender'],
	['eligibility','minimum_age'],
	['eligibility','maximum_age'],
	['primary_outcome','measure'],
	['secondary_outcome','measure'],
	['intervention','intervention_type'],
	['intervention','intervention_name'],
	['sponsors','lead_sponsor','agency'],
	['location','facility','name'],
	['provided_document_section','provided_document','document_date'],
	['provided_document_section','provided_document','document_type'],
	['provided_document_section','provided_document','document_url']
	]
"""


import importlib
gX = importlib.import_module('0-databaseManager')

import xml.etree.ElementTree as ET
def getRootFromString(string):
	return ET.fromstring(string)

def sanitizeText(txt):
	txt = str(txt)
	txt = txt.strip().replace('\n','| ')
	while "  " in txt: txt = txt.replace("  ","")
	txt.replace('\t','  ')
	#cutoff = 50
	#if len(txt) > cutoff: return txt[:cutoff-3]+"..."
	#if len(txt) == 0: return "______"
	while txt[0:2] == '[[': txt = txt[1:-1]
	if txt and txt[0] == '[' and txt.count('[') == 1: txt = txt[1:-1]
	return txt#[:cutoff]

def getChildText(root, key, keyidx=0):
	ret = ""
	
	if not isinstance(key, str): #must be list
		new_keyidx = keyidx + 1
		for new_root in root.findall(key[keyidx]):
			if new_keyidx == len(key)-1: key = key[-1]
			ret += "[" + getChildText(new_root, key, new_keyidx) + "]"
			ret += ", "
		return ret[:-2]

	for k in root.findall(key):
		ret += k.text + ", "
	if ret: ret = ret[:-2]
	return ret

def updateXmlDict(xmlDict, root, key):
	skey=str(key)
	xmlDict[skey] = sanitizeText(getChildText(root, key))

def multiUpdateXmlDict(xmlDict, root, keys):
	for key in keys: updateXmlDict(xmlDict, root, key)


keySet=set()
def genKeySet(root, pre_str=""):
	keySet.add(pre_str+root.tag)
	for child in root:
		if pre_str:
			genKeySet(child, pre_str + " > "+root.tag +" > ")
		else:
			genKeySet(child, root.tag +" > ")
	return




nctDict={}

for source in open('feed','r'):
	source = source.strip()
	xmlList = gX.getXMLlist(source)
	#xmlList = ['NCT03582800']
	
	for xmlFile in xmlList:
		xmlStr = gX.getXMLString(xmlFile)
		root = getRootFromString(xmlStr)
		xmlDict = {}
		#NCT-ID
		nct_id = root.find('id_info').find('nct_id').text
		#print(nct_id in nctDict)
		if nct_id in nctDict:
			#print(f"[FEED][NOTI][{nct_id}.xml] already processed, skipping...")
			continue
		
		
		multiUpdateXmlDict(xmlDict, root, keys)
		
		#updateXmlDict(xmlDict, root, 'official_title') #Title
		#if xmlDict['overall_status']=='Completed': continue #SKIP COMPLETED TRIALS
	
		#print(xmlDict)
		#print("\t","-->",getChildText(root, ['id_info','nct_id']))
		#print("\t","-->",getChildText(root, ['location','facility','name']))
		nctDict[nct_id]=xmlDict
		
		
		genKeySet(root) #Useful for seeing all possible keys

import os
from datetime import datetime
with open(outputTSV, 'w') as w:
	w.write(f'cm-tsv-quick-gen, branch: { os.popen("git describe --always").readlines()[0].strip() }-unstable, generated @ UTC: {datetime.utcnow().strftime("%Y-%m-%d, %H:%M:%S")}\n')
	
	for key in keys:
		key = str(key)
		w.write(key + '\t')
	w.write('\n')
	
	for nct_id in nctDict:
		for key in keys:
			key = str(key)
			w.write(nctDict[nct_id][key] + '\t')
		w.write('\n')

#for nct_id in nctDict:
#	print('overall_status')
#print(nctDict)


"""
#Print Datapoints
keyList = []
for key in keySet: keyList.append(key)
keyList.sort()
for i in keyList: print(i)
"""
