import importlib
gX = importlib.import_module('0-databaseManager')

import xml.etree.ElementTree as ET
def getRootFromString(string):
	return ET.fromstring(string)

def getRootFromFile(fileName):
	tree = ET.parse(fileName)
	return tree.getroot()

def sT(txt):
	txt = str(txt)
	txt = txt.strip().replace('\n',' ').replace('\t',' ')
	while "  " in txt: txt = txt.replace("  ","")
	cutoff = 50
	if len(txt) > cutoff: return txt[:cutoff-3]+"..."
	#if len(txt) == 0: return "______"
	return txt[:cutoff]

def recursivePrint(root, idx=0):
	for j in range(len(root)):
		child=root[j]
		for i in range(idx): print('\t',end="")
		print(f"{j}. {sT(child.tag)}: {sT(child.text)}")
		recursivePrint(child, idx+1)
	return


#xmlFile = './NCT03582800.xml'

#gX.setCache('cond=Psoriasis')
#xmlFile = './Data/extracted/'+gX.readInfoFile('cond=Psoriasis')[0]
#baseRoot = getRootFromFile(xmlFile)

#xmlList = []#gX.getXMLlist('cond=Psoriasis')
#

"""
for source in open('feed','r'):
	source = source.strip()
	gX.setCache(source)
"""

xmlFile = 'NCT00000112'
xmlStr = gX.getXMLString(xmlFile)
baseRoot = getRootFromString( xmlStr )
recursivePrint(baseRoot)

#print(baseRoot[1][1])
#print(baseRoot[1][1])
#print(baseRoot.find('id_info').find('nct_id'))
#print(baseRoot.find('id_info').find('nct_id').text)
#bR=baseRoot
#for u in bR: print(sT(u), sT(u.tag), sT(u.text), len(u))
#for child in baseRootNCT02998671:
	#print(f"{child.tag}: {child.text}")
	#recursivePrint(child)
